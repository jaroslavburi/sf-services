helm upgrade dev-a -f helm/values_a.yaml helm       --reuse-values --set appVersion="$VERSION" --set clusterEnv="dev" --set solacePass="$SOLACE_PASSWORD" --set dockerRegPass="$DOCKER_REGISTRY_PASS"
helm upgrade dev-b -f helm/values_b.yaml helm       --reuse-values --set appVersion="$VERSION" --set clusterEnv="dev" --set solacePass="$SOLACE_PASSWORD" --set dockerRegPass="$DOCKER_REGISTRY_PASS"
helm upgrade dev-c -f helm/values_c.yaml helm       --reuse-values --set appVersion="$VERSION" --set clusterEnv="dev" --set solacePass="$SOLACE_PASSWORD" --set dockerRegPass="$DOCKER_REGISTRY_PASS"
helm upgrade dev-ui -f helm/values_ui-dev.yaml helm --reuse-values --set appVersion="$VERSION" --set clusterEnv="dev" --set solacePass="$SOLACE_PASSWORD" --set dockerRegPass="$DOCKER_REGISTRY_PASS"

# helm install dev-a -f helm/values_a.yaml helm       --set appVersion="$VERSION" --set clusterEnv="dev" --set solacePass="$SOLACE_PASSWORD" --set dockerRegPass="$DOCKER_REGISTRY_PASS"
# helm install dev-b -f helm/values_b.yaml helm       --set appVersion="$VERSION" --set clusterEnv="dev" --set solacePass="$SOLACE_PASSWORD" --set dockerRegPass="$DOCKER_REGISTRY_PASS"
# helm install dev-c -f helm/values_c.yaml helm       --set appVersion="$VERSION" --set clusterEnv="dev" --set solacePass="$SOLACE_PASSWORD" --set dockerRegPass="$DOCKER_REGISTRY_PASS"
# helm install dev-ui -f helm/values_ui-dev.yaml helm --set appVersion="$VERSION" --set clusterEnv="dev" --set solacePass="$SOLACE_PASSWORD" --set dockerRegPass="$DOCKER_REGISTRY_PASS"
