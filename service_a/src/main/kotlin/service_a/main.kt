package service_a
import common.SolaceHandler

fun main() {
    var sh = SolaceHandler()
    sh.loadProperties("service_a")

    sh.standardService(
        sh.getProperty("subsribeToTopic"),
        sh.getProperty("publishToTopic"),
        sh.getProperty("publishMessage")
    )
}
