# Second Foundation Homework
## Required to be set:
`VERSION` - set in `.gitlab-ci.yml` used in gradle / helm / docker build
`CLUSTER_ENV` - defaults to dev

## Extras (compared to HW)
- Full CI/CD - deploy is automated in gitlab as well -> integration test 

## TODOs
- integration tests should use timestamp in the topic they push to so they can run concurrent
- finish fatJar implementation (not sure if its needed in this project)
- convert UI server to kolin
- - can easily reuse methods from common, just add endpoint
- - docker built easy
- more code comments (at least describe funs inputs/outputs/...)
- squash gitlab debug commits (should have been done on branch and merged/rebased into master as one commit)
- optimize helm values - lot of duplicate values in values_a/b/c.yaml - probably move to Chart.yaml?
- gitlab caching - commented - build takes 2 minutes 30s, caching of the build takes more than 5 minutes? (canceleld caching build) 
- test asserts are very weak - change for better library that at least shows excepted/actual
- create actual UI
- use real secure storage when using Secret env variables in gitlab (like www.vaultproject.io)
- create docker image with Az, kubectl, helm preinstalled to speed up deployments. Based on microsoft/azure-cli
- integration test should reuse already build jars from build step


## Endpoints
- dev UI
`http://20.76.0.80:8080/getLatest`
- uat UI
- prod UI

## Build app
- Non needed anymore, just set env VERSION Increment version
at `./build.gradle`
in `configure(project.subprojectList) {` set `version = "..."`
increment as well for deployment in `helm/Chart.yaml` at `appVersion`

- Gradle build, unitTest, generate dockers
`gradle build buildByConfig`

- Docker build and push - USE SAME VERSION AS IN APP
`docker login repo.treescale.com`
`VERSION=<version> cd ui-simple && docker build -t repo.treescale.com/jaroslavburi/sf-docker:ui-$VERSION . && docker push repo.treescale.com/jaroslavburi/sf-docker:ui-$VERSION`

## Deploy to Kubernetes on Azure:
- connect to Azure cluster to use for kubectl:
```
az aks get-credentials --resource-group newResourceGroup --name newDev
az aks get-credentials --resource-group newResourceGroup --name uat
az aks get-credentials --resource-group newResourceGroup --name prod
```

- create secret for registry repo.treescale.com deployment
```kubectl create secret docker-registry myregcred --docker-server=repo.treescale.com --docker-username=<> --docker-password=<> --docker-email=jaroslavburi@gmail.com```

- install/upgrade
dev
```
helm upgrade dev-a -f helm/values_a.yaml helm --set appVersion="$VERSION" --set image.env.CLUSTER_ENV="dev" --set image.env.SOLACE_PASSWORD="$SOLACE_PASSWORD" --set image.env.DOCKER_REGISTRY_PASS="$DOCKER_REGISTRY_PASS" &&
helm upgrade dev-b -f helm/values_b.yaml helm --set appVersion="$VERSION" --set image.env.CLUSTER_ENV="dev" --set image.env.SOLACE_PASSWORD="$SOLACE_PASSWORD" --set image.env.DOCKER_REGISTRY_PASS="$DOCKER_REGISTRY_PASS" &&
helm upgrade dev-c -f helm/values_c.yaml helm --set appVersion="$VERSION" --set image.env.CLUSTER_ENV="dev" --set image.env.SOLACE_PASSWORD="$SOLACE_PASSWORD" --set image.env.DOCKER_REGISTRY_PASS="$DOCKER_REGISTRY_PASS"
```

- redeploy
TODO - use on gitlab

- get services:
`kubectl get service`

- get pods -> All running:
`kubectl get pods --watch`

- desribe pod, ssh
`kubectl desribe pod ...`
`kubectl exec -i -t ... --container dev/... -- /bin/bash`

- get all events (debug loadBalancer)
`kubectl get events --all-namespaces`

- uninstall
`helm uninstall dev-a; helm uninstall dev-b; helm uninstall dev-c; helm uninstall dev-ui`

- list nodepool:
`az aks nodepool list --cluster-name newDev --resource-group newResourceGroup`

- stop cluster: (takes like 10 minutes)
`az aks stop --name newDev --resource-group newResourceGroup`

- start cluster: (takes like 10 minutes)
`az aks start --name newDev --resource-group newResourceGroup`

## Azure public ip for loadbalancer
- create IP (dev, uat, prod)
`export ENV=<env>`
```
az network public-ip create \
    --resource-group newResourceGroup \
    --name myAKSPublicIP-dev-ui \
    --allocation-method static \
    --sku standard
```

- get IP (for CICD automation)
`az network public-ip show --resource-group newResourceGroup --name myAKSPublicIP-dev1 --query ipAddress --output tsv`

- set IP
`set loadBalancer IP in helm/values-ui yaml`

- assign IP to resource
TODO: try PIP_RESOURCE_GROUP=$(az aks show --resource-group newResourceGroup --name newDev --query nodeResourceGroup -o tsv)
`./ui-simple/az-public_ip-dev.sh` (dev,uat,prod)
