package service_c
import common.SolaceHandler

fun main() {
    var sh = SolaceHandler()
    sh.loadProperties("service_c")

    sh.standardService(
        sh.getProperty("subsribeToTopic"),
        sh.getProperty("publishToTopic"),
        sh.getProperty("publishMessage")
    )
}
