package service_b
import common.SolaceHandler

fun main() {
    var sh = SolaceHandler()
    sh.loadProperties("service_b")

    sh.standardService(
        sh.getProperty("subsribeToTopic"),
        sh.getProperty("publishToTopic"),
        sh.getProperty("publishMessage")
    )
}
