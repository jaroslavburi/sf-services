
PIP_RESOURCE_GROUP=MC_newResourceGroup_newDev_westeurope
AKS_RESOURCE_GROUP=newResourceGroup
AKS_CLUSTER_NAME=newDev

CLIENT_ID=$(az aks show --resource-group $AKS_RESOURCE_GROUP --name $AKS_CLUSTER_NAME --query "servicePrincipalProfile.clientId" --output tsv)
echo $CLIENT_ID
SUB_ID=$(az account show --query "id" --output tsv)
echo $SUB_ID

az role assignment create\
    --assignee $CLIENT_ID \
    --role "Network Contributor" \
    --scope /subscriptions/$SUB_ID/resourceGroups/$PIP_RESOURCE_GROUP
