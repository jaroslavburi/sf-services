#!javascript
var solace = require('solclientjs');

var properties = new solace.SolclientFactoryProperties();
properties.profile = solace.SolclientFactoryProfiles.version10;
solace.SolclientFactory.init(properties);

var subscriber = {};
subscriber.log = function (line) {
    var now = new Date();
    var time = [('0' + now.getHours()).slice(-2), ('0' + now.getMinutes()).slice(-2), ('0' + now.getSeconds()).slice(-2)];
    var timestamp = '[' + time.join(':') + '] ';
    console.log(timestamp + line);
};

var solace_password = process.env.SOLACE_PASSWORD

subscriber.session = solace.SolclientFactory.createSession({
    url: "wss://mr-16jp1pl7u8tt.messaging.solace.cloud:443",
    vpnName: "sf-solace",
    userName: "solace-cloud-client",
    password: solace_password,
 });

 try {
    subscriber.session.connect();
 } catch (error) {
    subscriber.console.log(error);
 }

var globalSubsribeMessage = ""
subscriber.session.on(solace.SessionEventCode.UP_NOTICE, function (sessionEvent) {
    subscriber.log('=== Successfully connected and ready to subscribe. ===');
    subscriber.subscribe();
});

subscriber.session.on(solace.SessionEventCode.MESSAGE, function (message) {
    subscriber.log('Received message: "' + message.getBinaryAttachment() + '", details:\n' + message.dump());
    globalSubsribeMessage = message.getBinaryAttachment();
});


var env = ""
if(process.env.CLUSTER_ENV) { 
    env = process.env.CLUSTER_ENV
}else{
subscriber.log("CLUSTER_ENV NOT SET")
}

var config = require(`/tmp/git/git-sync/${env}/ui-config.js`);
// var config = require(`/usr/src/app/ui-config.js`); //Local debug

subscriber.subscribe = function () {
        try {
            subscriber.log("subsribing to " + config.subsribe_topic)
            subscriber.session.subscribe(
                solace.SolclientFactory.createTopic(config.subsribe_topic),
                true,
                config.subsribe_topic,
                10000
            );
        } catch (error) {
            subscriber.log("error in subsribe");
            subscriber.log(error.toString());
        }
    }

var express = require("express");
const e = require('express');
var app = express();
app.listen(8080, () => {
 console.log("Server running on port 8080");
});

app.get("/getLatest", (req, res, next) => {
    res.send(globalSubsribeMessage);
});
