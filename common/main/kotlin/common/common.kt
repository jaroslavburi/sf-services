package common
import com.solacesystems.jcsmp.*
import java.util.concurrent.CountDownLatch
import java.util.Properties
import java.io.FileInputStream

class SolaceHandler {
    var session:JCSMPSession? = null
    var latch = CountDownLatch(1)
    var cons:XMLMessageConsumer? = null
    var prod:XMLMessageProducer? = null
    var received_text:String = ""
    var properties = Properties();

    fun loadProperties(service_name: String){
        var env = System.getenv("CLUSTER_ENV") ?: "dev"
        properties.load(FileInputStream("/tmp/git/git-sync/$env/$service_name.properties"));
        
        var sh = SolaceHandler()
        sh.standardService(
        properties.getProperty("subsribeToTopic"),
        properties.getProperty("publishToTopic"), 
        properties.getProperty("publishMessage"))
    }

    fun getProperty(prop: String): String{
        return properties.get(prop) as String
    }

    fun standardService(subsriceTopic: String, publishTopic:String, publishMessage:String){
        println("properties loaded:")
        println(subsriceTopic)
        println(publishTopic)
        println(publishMessage)

        createSession()
        producerHandler()
        receiveHandler { publishToTopic(publishTopic, publishMessage) }
        subscribeToTopic(subsriceTopic)
    }

    fun createSession() {
        var solace_password = System.getenv("SOLACE_PASSWORD") ?: ""

        if (solace_password == ""){
            throw Exception("password not provided")
        }    
        // wss://mr-16jp1pl7u8tt.messaging.solace.cloud:8443 
        // MqttClient mqttClient = new MqttClient("ssl://mr-16jp1pl7u8tt.messaging.solace.cloud:8883", "HelloWorldSub");
        // MqttConnectOptions connectionOptions = new MqttConnectOptions();
        // connectionOptions.setUsername("solace-cloud-client");
        // connectionOptions.setPassword("n2e6g3lv4p0rvmrkf8qnr434pu".toCharArray())
        // mqttClient.connect(connectionOptions);

        var properties = JCSMPProperties()
        properties.setProperty(JCSMPProperties.HOST, "tcps://mr-16jp1pl7u8tt.messaging.solace.cloud:55443")
        properties.setProperty(JCSMPProperties.USERNAME, "solace-cloud-client")
        properties.setProperty(JCSMPProperties.VPN_NAME, "sf-solace")
        properties.setProperty(JCSMPProperties.PASSWORD, solace_password)
        properties.setProperty(JCSMPProperties.SSL_VALIDATE_CERTIFICATE, false)
        properties.setProperty(JCSMPProperties.MESSAGE_ACK_MODE, JCSMPProperties.SUPPORTED_MESSAGE_ACK_CLIENT)

        session = JCSMPFactory.onlyInstance().createSession(properties)
        session?.logFlowInfo(JCSMPLogLevel.DEBUG)
        session?.logSessionStats(JCSMPLogLevel.DEBUG)
        session?.connect()
    }

    //**************************** */
    //SUBSCRIBE
    //**************************** */
    fun receiveHandler(lambda: () -> Unit) {
        cons = session?.getMessageConsumer(object : XMLMessageListener {

            override fun onReceive(msg: BytesXMLMessage) {
                if (msg is TextMessage) {
                    var getMsg = msg.getText()
                    received_text = getMsg
                    println("TextMessage received: $getMsg")
                    lambda()
                } else {
                    println("Message received.")
                }
                latch.countDown()
            }

            override fun onException(e: JCSMPException) {
                System.out.printf("Consumer received exception: %s%n", e)
                latch.countDown()
            }
        })
    }

    fun subscribeToTopic(topicName: String) {
        var topic = JCSMPFactory.onlyInstance().createTopic(topicName)
        session?.addSubscription(topic)
        cons?.start()
        println("subsribed to topic $topicName")

        while (true) {
            try {
                latch.await()
            } catch (e: Exception) {
                println("ERROR ERROR!")
                println(e)
            }
        }
    }

    //**************************** */
    //PUBLISH
    //**************************** */
    fun producerHandler(){
        prod = session?.getMessageProducer(object: JCSMPStreamingPublishEventHandler {

            override fun responseReceived(messageID: String) {
                System.out.println("Producer received response for msg: " + messageID);
            }
        
            override fun handleError(messageID: String, e: JCSMPException, timestamp: Long) {
                System.out.printf("Producer received error for msg: %s@%s - %s%n",
                                    messageID,timestamp,e);
                }
        });
    }

    fun publishToTopic(topicName: String, additionalText: String) {
        var topic = JCSMPFactory.onlyInstance().createTopic(topicName);
        var msg = JCSMPFactory.onlyInstance().createMessage(TextMessage::class.java);
        msg.setText(received_text + additionalText);
        msg.setAckImmediately(true)
        println("publishing")
        println(topic)
        println(msg)
        println(prod)
        // println(session?.logSessionStats(JCSMPLogLevel.TRACE))
    
        
        prod?.send(msg,topic);
        prod?.close()
    }
}
