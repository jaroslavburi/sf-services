package unitTests

import org.junit.Rule
import org.junit.Test
import org.junit.contrib.java.lang.system.SystemOutRule
import kotlin.test.*
import com.solacesystems.jcsmp.*
import common.SolaceHandler
import org.awaitility.Awaitility.*
import java.util.concurrent.TimeUnit.*

class basicTests {

    @get:Rule
    public val systemOutRule:SystemOutRule = SystemOutRule().enableLog()

    @Test
    fun createSession(){
        var sh = SolaceHandler()
        sh.createSession()
        assertTrue(!sh.session!!.isClosed())
    }

    @Test
    fun smallIntegrationTest(){

        fun assertOutput():Boolean{
            var output = systemOutRule.getLog()
            println("thread output:" + output)
            return output.contains("unitTestMessage")
        }
        
        var sh = SolaceHandler()
        sh.createSession()
        sh.producerHandler()
        sh.receiveHandler { }

        Thread {
            println("subscribing")
            sh.subscribeToTopic("unitTest/unitTest")
        }.start()

        println("publishing")
        sh.publishToTopic("unitTest/unitTest", "unitTestMessage")

        await().atMost(40, SECONDS).until((::assertOutput))        
    }
}
