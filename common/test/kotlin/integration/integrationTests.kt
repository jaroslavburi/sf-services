package integrationTests

import org.junit.Rule
import org.junit.Test
import org.junit.contrib.java.lang.system.SystemOutRule
import kotlin.test.*
import com.solacesystems.jcsmp.*
import common.SolaceHandler
import org.awaitility.Awaitility.*
import java.util.concurrent.TimeUnit.*
import java.net.URLConnection
import java.net.URL
import java.net.*
import java.io.*

class basicTests {

    @get:Rule
    public val systemOutRule:SystemOutRule = SystemOutRule().enableLog()

    @Test
    fun IntegrationTest(){
        var inputLine = ""

        val charPool : List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')
        val randomString = (1..10)
        .map { i -> kotlin.random.Random.nextInt(0, 10) }
        .map(charPool::get)
        .joinToString("");
        val publishMessage = "integrationTest-" + randomString

        //TODO - should contain output from other services as well at the end
        fun assertOutput():Boolean{
            return inputLine.contains(publishMessage)
        }

        var sh = SolaceHandler()
        sh.createSession()
        sh.producerHandler()

        sh.publishToTopic("dev/final", publishMessage)

        var urlString = "http://20.76.0.80:8080/getLatest";
        var url = URL(urlString);
        var conn = url.openConnection();
        var buffer = BufferedReader(InputStreamReader(conn.getInputStream()))
        
        do {
            try{
            inputLine = buffer.readLine()
            }
            catch (e: Exception){break}            
        } while (inputLine != null)
            
        buffer.close();
        println(inputLine)
        await().atMost(40, SECONDS).until(::assertOutput)        

    }
}
